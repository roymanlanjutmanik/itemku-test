import React from "react";

import { Route, Switch } from "react-router-dom";

import Home from "../pages/Home";
import DetailPage from "../pages/DetailPage";

const Routes = () => {
  return (
    <Switch>
      <Route path="/:detail/:id" component={DetailPage} />
      <Route path="/" exact component={Home} />
    </Switch>
  );
};

export default Routes;
