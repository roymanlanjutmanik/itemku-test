import "./App.scss";
import "swiper/swiper.min.css";

import { BrowserRouter as Router, Route } from "react-router-dom";
import ReactNotification from "react-notifications-component";

import Routes from "./Routes/Routes";

function App() {
  return (
    <Router>
      <Route
        render={(props) => (
          <>
            <ReactNotification />
            <div className="App">
              <Routes />
            </div>
          </>
        )}
      />
    </Router>
  );
}

export default App;
