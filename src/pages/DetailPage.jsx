import React, { useState } from "react";
import { useParams } from "react-router";
import { Link } from "react-router-dom";

import H1 from "../components/atoms/H1";
import Image from "../components/atoms/Image";
import P from "../components/atoms/P";
import Button from "../components/atoms/Button";
import Card from "../components/molecules/Card";

import { HiOutlineShare } from "react-icons/hi";
import { AiOutlineArrowLeft, AiOutlineHeart } from "react-icons/ai";
import { FiShoppingCart } from "react-icons/fi";

import "react-notifications-component/dist/theme.css";
import { store } from "react-notifications-component";

import { data } from "../data/data";

import "./detailPage.scss";

const DetailPage = () => {
  const { id } = useParams();
  const items = data[id];
  const [isReadMore, setIsReadMore] = useState(false);
  const [addFavorite, setAddFavorite] = useState(false);

  const toggleReadMore = () => {
    setIsReadMore(!isReadMore);
  };

  const handleAddCard = () => {
    store.addNotification({
      title: "Berhasil",
      message: "Barang sudah dimasukkan ke keranjang! :D",
      type: "success",
      insert: "bottom",
      container: "center",
      animationIn: ["animate__animated", "animate__fadeIn"],
      animationOut: ["animate__animated", "animate__fadeOut"],
      dismiss: {
        duration: 5000,
        onScreen: true,
      },
      touchSlidingExit: {
        swipe: {
          duration: 400,
          timingFunction: "ease-out",
          delay: 0,
        },
        fade: {
          duration: 400,
          timingFunction: "ease-out",
          delay: 0,
        },
      },
    });
  };

  const handleErrorFeature = () => {
    store.addNotification({
      title: "Not found",
      message: "Maaf, fitur belum tersedia. :(",
      type: "info",
      insert: "bottom",
      container: "center",
      animationIn: ["animate__animated", "animate__fadeIn"],
      animationOut: ["animate__animated", "animate__fadeOut"],
      dismiss: {
        duration: 5000,
        onScreen: true,
      },
      touchSlidingExit: {
        swipe: {
          duration: 400,
          timingFunction: "ease-out",
          delay: 0,
        },
        fade: {
          duration: 400,
          timingFunction: "ease-out",
          delay: 0,
        },
      },
    });
  };

  return (
    <div className="detail-page">
     
      <div className="detail-div1">
        <div className="banner">
          <Image url={items.url} />
          <div className="overlay-icons">
            <Link to="/">
              <AiOutlineArrowLeft className="back-icon" />
            </Link>

            <div className="share-cart">
              <div onClick={() => handleErrorFeature()}>
                <HiOutlineShare />
              </div>
              <div onClick={() => handleErrorFeature()}>
                <FiShoppingCart />
              </div>
            </div>
          </div>
        </div>
        <div className="detail-price">
          <div className="item-game-wrapper">
            <div className="item-game">
              <H1 className={"item-game-detail"} content={items.itemName} />
              <P styles={"detail-game-name"} content={items.gameName} />
            </div>
            <div
              onClick={() => {
                setAddFavorite(!addFavorite);
              }}
            >
              <AiOutlineHeart
                className={addFavorite ? "heart-icon-active" : "heart-icon"}
              />
            </div>
          </div>

          <div className="discount-wrapper-detail">
            <P
              styles={"price"}
              content={
                items.discount
                  ? `Rp${((items.price * items.discount) / 100).toLocaleString(
                      "id"
                    )}`
                  : `Rp${items.price.toLocaleString("id")}`
              }
            />
            <P content={"per 1 Top Up"} />
          </div>
          <div className="discount-wrapper-detail">
            <P
              styles={"discount"}
              content={items.discount ? `${items.discount}% ` : ""}
            />
            <P
              content={
                items.discount ? (
                  <del>Rp{items.price.toLocaleString("id")}</del>
                ) : (
                  ""
                )
              }
            />
          </div>
        </div>
      </div>
      <div className="desc">
        <H1 content="Deskripsi Produk" />
        <P
          content={
            isReadMore ? `${items.desc}` : `${items.desc.slice(0, 150)}...`
          }
        />
        <Button
          label={isReadMore ? "Sembunyikan" : "Selengkapnya"}
          type={"button"}
          onClick={toggleReadMore}
          styles={"read-more-desc"}
        />
      </div>
      <div className="list-item">
        <div className="delivery-detail">
          <H1 content="Pengiriman Tercepat" />
          <Button
            label={"Lihat Semua"}
            type={"button"}
            styles={"read-more-delivery"}
          />
        </div>
        <P
          content={
            "Produk dari penjual-penjual yang memberi Garansi Pengiriman 10 menit"
          }
        />
        <Card item={data} />
      </div>
      <div className="btn-cart">
        <Button
          onClick={() => handleAddCard()}
          type={"button"}
          styles={"add-to-cart"}
          label={"Tambah Ke Troli"}
        />
      </div>
    </div>
  );
};

export default DetailPage;
