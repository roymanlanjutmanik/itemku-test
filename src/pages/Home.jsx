import React from "react";

import ListItem from "../components/organisms/ListItem";
import Header from "../components/organisms/Header";
import Footer from "../components/organisms/Footer";

const Home = () => {
  return (
    <div>
      <Header />
      <ListItem />
      <Footer />
    </div>
  );
};

export default Home;
