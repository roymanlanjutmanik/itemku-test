import React from "react";

import "./footer.scss";

import P from "../atoms/P";

const Footer = () => {
  return (
    <div className="footer">
      <P content={"© 2014 - 2021 PT. Five Jack All Rights Reserved."}></P>
      <P
        content={"All other trademarks belong to their respective owners."}
      ></P>
    </div>
  );
};

export default Footer;
