import React from "react";
import { Link } from "react-router-dom";

import Image from "../atoms/Image";

import logo from "../../assets/itemku-logo.png";

import "./header.scss";

const Header = () => {
  return (
    <div className="header">
      <Link to="/" className="logo">
        <Image url={logo} />
      </Link>
    </div>
  );
};

export default Header;
