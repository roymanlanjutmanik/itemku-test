import React from "react";
import PropTypes from "prop-types";
import { Swiper, SwiperSlide } from "swiper/react";

import P from "../atoms/P";
import { Link } from "react-router-dom";

import "./card.scss";

const Card = (props) => {
  const items = props.item;

  return (
    <Swiper grabCursor={true} spaceBetween={10} slidesPerView={"auto"}>
      {items.map((el) => (
        <SwiperSlide key={el.id}>
          <Link to={"/detail/" + el.id}>
            <div
              className="bg"
              style={{ backgroundImage: `url(${el.url})` }}
            ></div>
            <div className="detail">
              <P styles={"bold"} content={el.itemName} />
              <P styles={"thin"} content={el.gameName} />
              <P
                styles={
                  el.availibility === "available" ? "available" : "limited"
                }
                content={
                  el.availibility === "available"
                    ? `Stok ${el.stock}+`
                    : `Stok ${el.stock}`
                }
              />
              <div className="discount-wrapper">
                <P
                  styles={"discount"}
                  content={el.discount ? `${el.discount}% ` : ""}
                />
                <P
                  content={
                    el.discount ? (
                      <del>Rp{el.price.toLocaleString("id")}</del>
                    ) : (
                      ""
                    )
                  }
                />
              </div>
              <P
                styles={"price"}
                content={
                  el.discount
                    ? `Rp${((el.price * el.discount) / 100).toLocaleString(
                        "id"
                      )}`
                    : `Rp${el.price.toLocaleString("id")}`
                }
              />
              <P styles={"delivery"} content={el.deliveryBadge} />{" "}
            </div>
            <P
              styles={"total-sold"}
              content={`${el.totalSold} produk terjual`}
            />
          </Link>
        </SwiperSlide>
      ))}
    </Swiper>
  );
};

Card.propTypes = {
  props: PropTypes.object,
};

export default Card;
