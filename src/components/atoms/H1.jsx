import React from "react";
import PropTypes from "prop-types";

const H1 = ({ content, className }) => {
  return <h1 className={className}>{content}</h1>;
};

H1.propTypes = {
  content: PropTypes.string,
  className: PropTypes.string,
};

export default H1;
