import React from "react";
import PropTypes from "prop-types";

import "./p.scss";

const P = ({ content, styles }) => {
  return <p className={styles}>{content}</p>;
};

P.propTypes = {
  content2: PropTypes.any,
  content: PropTypes.any,
  styles: PropTypes.string,
};

export default P;
