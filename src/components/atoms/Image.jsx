import React from "react";
import PropTypes from "prop-types";

// import logo from '../../assets/'
import "./images.scss";

const Image = ({ url, className }) => {
  return <img className={className} src={url} alt={url} />;
};

Image.propTypes = {
  url: PropTypes.string,
  className: PropTypes.string,
};

export default Image;
