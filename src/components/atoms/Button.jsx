import React from "react";
import PropTypes from "prop-types";

import "./button.scss";

const Button = ({ label, type, onClick, styles }) => {
  return (
    <button className={styles} type={type} onClick={onClick}>
      {label}
    </button>
  );
};

Button.propTypes = {
  label: PropTypes.string,
  type: PropTypes.string,
  onClick: PropTypes.func,
  styles: PropTypes.string,
};

export default Button;
