# About Project

This project is Itemku's test as part of their recruitment process.
To run this project, simply install all the dependencies and run the
app using npm run start.

## Folder structure

- All the components are made inside of components Folder following 
atomic design principle.
- All the routes are inside Routes Folder.
- All the data used are inside data Folder.

